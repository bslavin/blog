class Post < ActiveRecord::Base
  has_many :comments, dependent: :destroy
  validates :title, presence: true
  validates :body, presence: true
end

  Post.create(title: nil).valid?
  Post.create(body: nil).valid?
